import "./App.css";
import { useState } from "react";
import RestrictedPage from "./Components/RestrictedPage";

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const user = "Miqueias";

  const logIn = () => {
    setIsLoggedIn(true);
  };
  const logOut = () => {
    setIsLoggedIn(false);
  };

  return (
    <div className="App">
      <header className="App-header">
        <RestrictedPage
          isLoggedIn={isLoggedIn}
          user={user}
          Login={logIn}
          Logout={logOut}
        />
      </header>
    </div>
  );
}

export default App;
