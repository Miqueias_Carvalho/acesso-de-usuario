import "./style.css";

function RestrictedPage({ isLoggedIn, user, Login, Logout }) {
  return (
    <div className="log">
      {isLoggedIn ? (
        <div className="logado">
          <p>
            Bem-vindo <span>{user}</span> !{" "}
          </p>
          <button onClick={Logout}>Sair</button>
        </div>
      ) : (
        <div className="deslogado">
          <p>
            Você <strong>não pode acessar</strong> essa página !
          </p>
          <button onClick={Login}>Entrar</button>
        </div>
      )}
    </div>
  );
}
export default RestrictedPage;
